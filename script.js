function SaqueProTeste(Saque, ValorSaldo, Quantidadenotas) {
  const saldo = ValorSaldo;
  let total = Saque;
  let retorno = {};
  const notas = Quantidadenotas;
  const notasUsadas = [100, 50, 20, 10, 5, 2, 1];
  if (Saque > saldo) {
    return "Informar que não possui saldo";
  }
  if (Saque < 1) {
    return "O saque deve ser maior que 0";
  }

  function CountNotas(valor) {
    for (let i = total / valor; i > 0.99; i--) {
      if (total >= valor) {
        if (notas[valor] !== 0) {
          notas[valor] -= 1;
          if (retorno[valor] === undefined) {
            retorno[valor] = 0;
          }
          retorno[valor] += 1;
          total -= valor;
        }
      }
    }
  }

  notasUsadas.map((item) => {
    CountNotas(item);
  });

  let teste = [];
  let result = "Entregar";

  function toString() {
    for (let i = 0; i < 6; i++) {
      valor = notasUsadas[i];
      if (retorno[valor] === undefined) {
      } else if (valor === 1) {
      } else {
        teste.push(` ${retorno[valor]} nota de R$${valor},00`);
      }
    }
    if (teste.length === 1) {
      result += teste[0];
    } else {
      teste.map((item) => {
        if (item === teste[teste.length - 1]) {
          result += " e" + item;
        } else if (item === teste[teste.length - 2]) {
          result += item;
        } else {
          result += item + ",";
        }
      });
    }
  }
  toString();

  return result;
}

let button = document.querySelector("#button");

button.addEventListener("click", function (e) {
  e.preventDefault();
  const value = document.querySelector("#saque");
  const localResult = document.querySelector("#resultado");
  if (localResult.firstChild !== null) {
    localResult.removeChild(localResult.firstChild);
  }
  let notas = document.createElement("p");

  notas.innerHTML = SaqueProTeste(value.value, 122.5, {
    100: 1,
    50: 3,
    20: 4,
    10: 1,
    5: 3,
    2: 12,
    1: 10,
  });
  localResult.appendChild(notas);
});
